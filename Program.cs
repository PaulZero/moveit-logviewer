﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LogThing.ValueObjects;
using Renci.SshNet;

namespace LogThing
{
    class Program
    {
        private const string AllowStupidLogSizesFlag = "--ignoreLogSize";
        
        private const string PrivateKeyPath
            = "/home/paul/MoveIt/.vagrant/machines/api.moveit.local/virtualbox/private_key";

        private static FileSize MaxAllowedFileSize { get; } = FileSize.FromKiloBytes(100); 

        private const string LogPath = "/var/log/psg/core-api_dev.log";


        private static async Task Main(string[] args)
        {
            var allowStupidLogSizes = !args.Contains(AllowStupidLogSizesFlag);
            
            var server = new LogServer();

            try
            {
                var mainTask = Task.Run(async () =>
                {
                    try
                    {                        
                        server.Start();

                        using (var privateKey = new PrivateKeyFile(PrivateKeyPath))
                        using (var connection = new SshClient("192.168.37.10", 22, "vagrant", privateKey))
                        {
                            connection.ConnectionInfo.Timeout = TimeSpan.FromHours(8);
                            connection.Connect();

                            using (var statCommand = connection.RunCommand($"stat --printf=\"%s\" {LogPath}"))
                            using (var tailCommand = connection.CreateCommand("tail -f -n +1 /var/log/psg/core-api_dev.log"))
                            {
                                var size = new FileSize(statCommand.Result);

                                if (size > MaxAllowedFileSize && !allowStupidLogSizes)
                                {
                                    Console.WriteLine(" * Error:");
                                    Console.WriteLine($"    File is greater than {MaxAllowedFileSize} (actual size is {size.MegaBytes} MB)");
                                    Console.WriteLine($"    To process a log of this size you must specify the {AllowStupidLogSizesFlag} flag.");

                                    return;
                                }
                                
                                Console.WriteLine($" * Loading log file: '{LogPath}' ({size})");

                                var result = tailCommand.BeginExecute();

                                var bytesProcessed = new FileSize(0);

                                using (var reader = new StreamReader(tailCommand.OutputStream))
                                {
                                    Console.WriteLine(" * Tailing log...");
                                    
                                    while (!result.IsCompleted)
                                    {
                                        var line = await reader.ReadLineAsync();

                                        if (string.IsNullOrWhiteSpace(line))
                                        {
                                            continue;
                                        }

                                        bytesProcessed = bytesProcessed + reader.CurrentEncoding.GetByteCount(line);

                                        if (string.IsNullOrWhiteSpace(line))
                                        {
                                            await Task.Delay(TimeSpan.FromMilliseconds(100));

                                            continue;
                                        }

                                        if (LogRow.TryCreateLogRow(line, out var logRow))
                                        {
                                            server.AddLogRow(logRow);
                                        }
                                        
                                        Console.Write($"\r{bytesProcessed} / {size}");
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine($"Error caught in main process: {exception} {exception.Message}");
                    }

                    Console.WriteLine("Finishing main task?!");
                });

                Console.WriteLine("Server now running...");

                // Wait a second to allow it to actually load some bloody log files...
                await Task.Delay(TimeSpan.FromSeconds(3));
                
                Console.WriteLine("http://localhost:14800/");

                await mainTask;

                if (mainTask.IsFaulted)
                {
                    Console.WriteLine($"Server has halted due to an error: {mainTask.Exception.Message}");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Server failed: {exception.Message}");
            }
            finally
            {
                Console.WriteLine("Stopping server...");

                server.Stop();
            }

            Console.WriteLine("Server has stopped.");
        }
    }
}