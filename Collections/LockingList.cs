﻿using System.Collections.Generic;

namespace LogThing.Collections
{
    public class LockingList<T>
    {
        private readonly List<T> internalList = new List<T>();
        
        private readonly object lockingObject = new object();

        public void Add(T item)
        {
            lock (lockingObject)
            {
                internalList.Add(item);
            }
        }

        public List<T> GetCopy()
        {
            lock (lockingObject)
            {
                return new List<T>(internalList);
            }
        }
    }
}