﻿using System;

namespace LogThing.ValueObjects
{
    public struct FileSize
    {
        public const double BytesPerKiloByte = 1024;

        public const double BytesPerMegaByte = BytesPerKiloByte * 1024;

        public const double BytesPerGigaByte = BytesPerMegaByte * 1024;

        public const double BytesPerTeraByte = BytesPerGigaByte * 1024;

        public double Bytes { get; }

        public double KiloBytes { get; }

        public double MegaBytes { get; }

        public double GigaBytes { get; }

        public double TeraBytes { get; }

        public FileSize(double bytes)
        {
            Bytes = bytes;
            KiloBytes = Math.Round(Bytes / BytesPerKiloByte, 2);
            MegaBytes = Math.Round(Bytes / BytesPerMegaByte, 2);
            GigaBytes = Math.Round(Bytes / BytesPerGigaByte, 2);
            TeraBytes = Math.Round(Bytes / BytesPerTeraByte, 2);
        }

        public FileSize(string bytes) : this(double.Parse(bytes))
        {
        }

        public static bool operator >(FileSize sizeOne, FileSize sizeTwo)
        {
            return sizeOne.Bytes > sizeTwo.Bytes;
        }

        public static bool operator <(FileSize sizeOne, FileSize sizeTwo)
        {
            return sizeOne.Bytes < sizeTwo.Bytes;
        }

        public static FileSize operator +(FileSize size, double extraBytes)
        {
            return new FileSize(size.Bytes + extraBytes);
        }

        public static FileSize operator +(FileSize sizeOne, FileSize sizeTwo)
        {
            return new FileSize(sizeOne.Bytes + sizeTwo.Bytes);
        }

        public override string ToString()
        {
            var sizes = new[] {"B", "KB", "MB", "GB", "TB"};
            var length = (float) Bytes;
            var order = 0;

            while (length >= 1024 && order < sizes.Length - 1)
            {
                order++;
                length = length / 1024;
            }

            return string.Format("{0:0.##} {1}", length, sizes[order]);
        }

        public static FileSize FromKiloBytes(double kiloBytes)
        {
            return new FileSize(kiloBytes * BytesPerKiloByte);
        }

        public static FileSize FromMegaBytes(double megaBytes)
        {
            return new FileSize(megaBytes * BytesPerMegaByte);
        }

        public static FileSize FromGigaBytes(double gigaBytes)
        {
            return new FileSize(gigaBytes * BytesPerGigaByte);
        }

        public static FileSize FromTeraBytes(double teraBytes)
        {
            return new FileSize(teraBytes * BytesPerTeraByte);
        }
    }
}