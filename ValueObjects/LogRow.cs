﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LogThing.ValueObjects
{
    public class LogRow
    {
        [JsonIgnore]
        public static Regex LogRowPattern = new Regex(
            @"^\[(?<timestamp>\d{4}\-\d{2}\-\d{2} \d{2}\:\d{2}\:\d{2})\] (?<logName>[a-zA-Z\-_]{1,})\.(?<logLevel>[A-Z]{1,})\: (?<logText>.*)$");

        [JsonIgnore]
        public static Regex LogTextJsonPattern = new Regex(
            @"^(?<message>.*) (?<json>[\{\(]{1}.*[\}\]]{1})$");

        public Guid Guid { get; } = Guid.NewGuid();

        [JsonIgnore]
        public DateTime Timestamp { get; }

        [JsonProperty("Timestamp")]
        public string FormattedTimestamp => Timestamp.ToString("G");

        public string LogName { get; }

        public string LogLevel { get; }

        public string LogText { get; }

        [JsonIgnore]
        public string Json { get; }
        
        [JsonIgnore]
        public string StackTrace { get; private set; }

        public bool HasJson => !string.IsNullOrWhiteSpace(Json);

        public bool HasStackTrace => !string.IsNullOrWhiteSpace(StackTrace);

        [JsonIgnore]
        public string SearchText { get; }

        public LogRow(DateTime timestamp, string logName, string logLevel, string logText)
        {
            Timestamp = timestamp;
            LogName = logName;
            LogLevel = logLevel;
            LogText = logText.Trim();

            // Get the last index of [ then substring to that to remove the dead [] and the space before it.
            LogText = LogText.Substring(0, LogText.LastIndexOf('[') - 1);

            // if string ends in [] now then fuck that off too.
            if (LogText.EndsWith(" []"))
            {
                LogText = LogText.Substring(0, LogText.LastIndexOf('[') - 1);
            }
            else
            {
                // Check to see if it contains some JSON, if so we can populate that as well..

                if (LogText.Contains("{") && LogText.Contains("}"))
                {
                    var startOfJson = LogText.IndexOf("{", StringComparison.Ordinal);
                    var rawJson = LogText.Substring(startOfJson);
                    LogText = LogText.Substring(0, startOfJson - 1);

                    if (JsonConvert.DeserializeObject(rawJson) is JToken actualJson)
                    {
                        var passwordPaths = new List<string>();

                        ParseJson(actualJson, passwordPaths, out var stackTracePath);

                        // If a stack trace exists within the log row, purge it so we're not sending
                        // it back twice in the response!
                        if (HasStackTrace && actualJson?.SelectToken(stackTracePath) is JValue stackTraceValue)
                        {
                            stackTraceValue.Value = "...";
                        }

                        // For each password that was located within the JSON ensure that it's purged
                        // so they cannot be harvested from the logs.
                        foreach (var passwordPath in passwordPaths)
                        {
                            if (actualJson?.SelectToken(passwordPath) is JValue passwordValue)
                            {
                                passwordValue.Value = "...";
                            }
                        }

                        Json = actualJson.ToString(Formatting.Indented);
                    }
                }
            }

            SearchText = LogText + Json;
        }

        public bool ContainsString(string value)
        {
            return SearchText.Contains(value.ToLower());
        }

        public static bool TryCreateLogRow(string rawLogRow, out LogRow logRow)
        {
            var match = LogRowPattern.Match(rawLogRow);

            if (match.Success)
            {
                var timestamp = DateTime.Parse(match.Groups["timestamp"].Value);
                var logName = match.Groups["logName"].Value;
                var logLevel = match.Groups["logLevel"].Value;
                var logText = match.Groups["logText"].Value;

                logRow = new LogRow(timestamp, logName, logLevel, logText);

                return true;
            }

            logRow = null;

            return false;
        }

        private void ParseJson(JToken token, ICollection<string> passwordPaths, out string stackTracePath)
        {
            stackTracePath = "";
            
            if (token is JProperty property)
            {
                if (property.Value is JValue value)
                {
                    var name = property.Name.ToLower();

                    if (new[] {"stacktrace", "stack trace"}.Contains(name))
                    {
                        StackTrace = value.Value?.ToString();

                        stackTracePath = property.Path;
                    }
                    
                    if (name == "password")
                    {
                        passwordPaths.Add(property.Path);
                    }

                    return;
                }
            }

            foreach (var childToken in token.Children())
            {
                ParseJson(childToken, passwordPaths, out stackTracePath);
            }
        }
    }
}