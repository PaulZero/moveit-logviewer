﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Antlr4.StringTemplate;
using LogThing.Collections;
using LogThing.Exceptions;
using LogThing.Responses;
using LogThing.ValueObjects;
using Newtonsoft.Json;

namespace LogThing
{
    public class LogServer
    {
        private TcpListener Listener { get; }

        private LockingList<LogRow> LogRows { get; }
            = new LockingList<LogRow>();

        private CancellationTokenSource TokenSource { get; }
            = new CancellationTokenSource();

        public LogServer()
        {
            Listener = new TcpListener(IPAddress.Any, 14800);

            Listener.Server.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1);
        }

        public void AddLogRow(LogRow logRow)
        {
            LogRows.Add(logRow);
        }

        public void Start()
        {
            Listener.Start();

            Task.Run(async () =>
            {
                while (!TokenSource.IsCancellationRequested)
                {
                    try
                    {
                        using (var context = await Listener.AcceptTcpClientAsync())
                        using (var reader = new StreamReader(context.GetStream()))
                        using (var writer = new StreamWriter(context.GetStream()) {NewLine = "\r\n"})
                        {
                            var requestText = await reader.ReadLineAsync();

                            if (!requestText.StartsWith("GET"))
                            {
                                throw new BadRequestException();
                            }

                            Console.WriteLine("Request received.");
                            Console.WriteLine(requestText);

                            var requestParts = requestText.Split(' ');
                            var requestPath = requestParts[1];

                            var parameters = new NameValueCollection();

                            if (requestPath.Contains('?'))
                            {
                                var splitPath = requestPath.Split('?');

                                requestPath = splitPath[0];

                                var queryString = splitPath[1];

                                parameters = HttpUtility.ParseQueryString(queryString);
                            }

                            var responseFactory = new ResponseFactory();

                            var response = responseFactory.CreateResponse(requestPath, LogRows.GetCopy());
                            
                            try
                            {
                                SendResponse(response.Render(requestPath, parameters), response.ContentType, writer);
                            }
                            catch (BadRequestException)
                            {
                                SendResponse("", "text/html", "400 Bad Request", writer);
                            }
                            catch (NotFoundException)
                            {
                                SendResponse("", "text/html", "404 Not Found", writer);
                            }

                            Console.WriteLine("Request completed.");
                        }

                        await Task.Delay(TimeSpan.FromMilliseconds(100));
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine($"Request error occurred: {exception.Message}");
                        Console.WriteLine(exception.StackTrace);
                    }
                }
            }, TokenSource.Token);
        }

        public void Stop()
        {
            TokenSource.Cancel();
            
            Listener.Stop();
        }

        private void SendResponse(string content, string contentType, StreamWriter writer)
        {
            SendResponse(content, contentType, "200 OK", writer);
        }

        private void SendResponse(string content, string contentType, string statusMessage, StreamWriter writer)
        {
            var responseText = content.Trim();
            var responseLength = Encoding.UTF8.GetByteCount(responseText);
                            
            writer.WriteLine($"HTTP/1.1 {statusMessage}");
            writer.WriteLine($"Content-Length: {responseLength}");
            writer.WriteLine($"Content-Type: {contentType}; charset=utf-8");
            writer.WriteLine();
            writer.Write(responseText);
        }
    }
}