﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using LogThing.ValueObjects;
using Newtonsoft.Json;

namespace LogThing.Responses
{
    public class QueriedDataResponse : BaseResponse
    {
        public const int DefaultPageSize = 25;

        public override string ContentType { get; } = "application/json";

        public QueriedDataResponse(List<LogRow> logRows) : base(logRows)
        {
        }

        public override bool IsMatch(string requestPath)
        {
            return requestPath == "/query";
        }

        public override string Render(string requestPath, NameValueCollection parameters)
        {
            var filteredRows = LogRows.AsEnumerable();

            var pageNumber = 0;
            var requestedPageSize = DefaultPageSize;

            foreach (var key in parameters.AllKeys)
            {
                var value = parameters[key];

                if (string.IsNullOrWhiteSpace(value))
                {
                    continue;
                }

                switch (key)
                {
                    case "todayOnly":
                        filteredRows = filteredRows.Where(r => r.Timestamp >= DateTime.Today);
                        break;

                    case "logName":
                        filteredRows = filteredRows.Where(r => r.LogName == value);
                        break;

                    case "logLevel":
                        filteredRows = filteredRows.Where(r => r.LogLevel == value);
                        break;

                    case "searchQuery":
                        filteredRows = filteredRows.Where(r => r.ContainsString(value));
                        break;

                    case "pageNumber":
                        if (int.TryParse(value, out var newPageNumber))
                        {
                            // We accept page numbers starting at 1, but it's easier starting from 0...
                            pageNumber = (newPageNumber == 0) ? 0 : newPageNumber - 1;
                        }

                        break;
                    case "pageSize":
                        if (int.TryParse(value, out var newPageSize))
                        {
                            requestedPageSize = newPageSize;
                        }

                        break;
                }
            }

            // Sort it so latest come first
            filteredRows = filteredRows.OrderByDescending(r => r.Timestamp);
            
            var pageSize = requestedPageSize;
            var totalRows = filteredRows.Count();
            var totalPages = (int) Math.Floor((double) totalRows / pageSize);

            if (pageNumber >= totalPages)
            {
                pageNumber = totalPages;
                pageSize = pageSize - ((totalPages * pageSize) - totalRows);
            }

            var pageRows = filteredRows.Skip(pageNumber * requestedPageSize).Take(pageSize);

            // Reset the page number back to +1 so as not to confuse the humans...
            pageNumber++;

            // Humans also don't like to be told they are on page 1 of 0...
            if (totalPages == 0)
            {
                totalPages++;
            }

            return JsonConvert.SerializeObject(new
            {
                ok = true,
                data = pageRows.ToArray(),
                paging = new
                {
                    totalPages,
                    totalRows,
                    pageNumber
                }
            });
        }
    }
}