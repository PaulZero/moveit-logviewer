﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using LogThing.ValueObjects;
using Newtonsoft.Json;

namespace LogThing.Responses
{
    public class RangedDataResponse : BaseResponse
    {
        public override string ContentType { get; } = "application/json";

        private Regex RouteRegex { get; }
            = new Regex(@"^\/from\/(?<firstIndex>[0-9]{1,})\/to\/(?<lastIndex>[0-9]{1,})$");
        
        public RangedDataResponse(List<LogRow> logRows) : base(logRows)
        {
        }

        public override bool IsMatch(string requestPath)
        {
            return RouteRegex.IsMatch(requestPath);
        }

        public override string Render(string requestPath, NameValueCollection parameters)
        {
            var match = RouteRegex.Match(requestPath);
            var firstIndex = int.Parse(match.Groups["firstIndex"].Value);
            var lastIndex = int.Parse(match.Groups["lastIndex"].Value);
            var count = lastIndex - firstIndex;

            if (count <= 0)
            {
                return JsonConvert.SerializeObject(new
                {
                    ok = false,
                    error = "Invalid range specified."
                });
            }

            var rows = LogRows.GetRange(firstIndex, count);

            return JsonConvert.SerializeObject(new
            {
                ok = true,
                data = rows
            });
        }
    }
}