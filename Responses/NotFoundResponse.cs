﻿using System.Collections.Generic;
using System.Collections.Specialized;
using LogThing.ValueObjects;

namespace LogThing.Responses
{
    public class NotFoundResponse : BaseResponse
    {
        public NotFoundResponse(List<LogRow> logRows) : base(logRows)
        {
        }

        public override bool IsMatch(string requestPath)
        {
            return true;
        }

        public override string Render(string requestPath, NameValueCollection parameters)
        {
            return "Page not found.";
        }
    }
}