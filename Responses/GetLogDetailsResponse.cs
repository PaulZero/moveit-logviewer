﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using LogThing.Exceptions;
using LogThing.ValueObjects;
using Newtonsoft.Json;

namespace LogThing.Responses
{
    public class GetLogDetailsResponse : BaseResponse
    {
        public GetLogDetailsResponse(List<LogRow> logRows) : base(logRows)
        {
        }

        public override bool IsMatch(string requestPath)
        {
            return requestPath == "/logDetails";
        }

        public override string Render(string requestPath, NameValueCollection parameters)
        {
            if (!parameters.AllKeys.Contains("guid"))
            {
                throw new BadRequestException();
            }

            var logRow = LogRows.FirstOrDefault(r => r.Guid.ToString() == parameters["guid"]);

            if (logRow is null)
            {
                throw new NotFoundException();
            }

            return JsonConvert.SerializeObject(new
            {
                ok = true,
                logRow.Json,
                logRow.StackTrace
            });
        }
    }
}