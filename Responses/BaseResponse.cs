﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using Antlr4.StringTemplate;
using LogThing.ValueObjects;

namespace LogThing.Responses
{
    public abstract class BaseResponse
    {
        public virtual string ContentType { get; } = "text/html";
        
        protected List<LogRow> LogRows { get; }

        protected BaseResponse(List<LogRow> logRows)
        {
            LogRows = logRows;
        }
        
        public abstract bool IsMatch(string requestPath);

        public abstract string Render(string requestPath, NameValueCollection parameters);

        protected string GetViewScript(string path)
        {
            var executablePath = new FileInfo(Assembly.GetExecutingAssembly().Location);
            var rootDirectory = executablePath.Directory;
            var templateDirectory = rootDirectory.GetDirectories().FirstOrDefault(d => d.Name == "templates");

            if (templateDirectory is null)
            {
                throw new Exception($"Could not find template directory from {executablePath}");
            }

            return File.ReadAllText(Path.Combine(templateDirectory.FullName, path));
        }
    }
}