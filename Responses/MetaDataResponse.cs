﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using LogThing.ValueObjects;
using Newtonsoft.Json;

namespace LogThing.Responses
{
    public class MetaDataResponse : BaseResponse
    {
        public override string ContentType { get; } = "application/json";

        public MetaDataResponse(List<LogRow> logRows) : base(logRows)
        {
        }

        public override bool IsMatch(string requestPath)
        {
            return requestPath == "/meta";
        }

        public override string Render(string requestPath, NameValueCollection parameters)
        {
            var logNames = LogRows.Select(r => r.LogName).Distinct().ToArray();
            var logLevels = LogRows.Select(r => r.LogLevel).Distinct().ToArray();

            return JsonConvert.SerializeObject(new
            {
                ok = true,
                data = new
                {
                    names = logNames,
                    levels = logLevels
                }
            });
        }
    }
}