﻿using System;
using System.Collections.Generic;
using LogThing.ValueObjects;

namespace LogThing.Responses
{
    public class ResponseFactory
    {
        public BaseResponse CreateResponse(string requestPath, List<LogRow> logRows)
        {
            BaseResponse[] allowedResponses =
            {
                new DefaultResponse(logRows),
                new RangedDataResponse(logRows),
                new QueriedDataResponse(logRows),
                new MetaDataResponse(logRows),
                new GetLogDetailsResponse(logRows), 
            };

            foreach (var response in allowedResponses)
            {
                if (response.IsMatch(requestPath))
                {
                    return response;
                }
            }

            return new NotFoundResponse(logRows);
        }
    }
}