﻿using System.Collections.Generic;
using System.Collections.Specialized;
using LogThing.ValueObjects;

namespace LogThing.Responses
{
    public class DefaultResponse : BaseResponse
    {
        public DefaultResponse(List<LogRow> logRows) : base(logRows)
        {
        }

        public override bool IsMatch(string requestPath)
        {
            return requestPath == "/";
        }

        public override string Render(string requestPath, NameValueCollection parameters)
        {
            return GetViewScript("bootstrap.html");
        }
    }
}